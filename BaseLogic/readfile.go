package BaseLogic

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func Massiv() {
	/*
		numbers := [7]int{1, 2, 3, 6, 4, 3, 9}
		var sum = 0
		for _, numbers := range numbers {
			sum += numbers
		}
		simpleCount := len(numbers)
		fmt.Printf("Avverage: %.1v\n", sum/simpleCount)


	*/
	/*
		numbers := [6]int{3, 16, -2, 10, 23, 12}
		for i, number := range numbers {
			if number >= 10 && number <= 20 {
				fmt.Println(i, number)
			}
		}


	*/
	/*
		file, err := os.Create("data.txt")
		if err != nil {
			log.Fatal(err)
			os.Exit(1)
		}
		defer file.Close()
		fmt.Println(file.Name())

	*/
}

func OpenFile() {
	file, err := os.Open("data.txt")
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
	err = file.Close()
	if err != nil {
		log.Fatal(err)
	}
	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
}
