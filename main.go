package main

import (
	p "ProjectLessons1/BaseLogic"
	"fmt"
	"log"
)

func main() {
	Show()
	Counts()
}

func Show() {

	//p.TestPass()
	//p.Game()
	//p.DoubleNumber(4)
	//var amount, total float64
	//amount = p.PaintNeeded(4.2, 3.0)
	//fmt.Printf("%0.2f liters needed\n", amount)
	//total += amount
	//amount = p.PaintNeeded(5.2, 3.5)
	//fmt.Printf("%0.2f liters needed\n", amount)
	//total += amount
	//fmt.Printf("Total: %0.2f liters\n", total)
	/*
		amount, err := p.PaintNeeded(4.2, -3.0)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Printf("%0.2f liters needed\n", amount)
		}

	*/
	/*
		root, err := p.SquareRoot(9.3)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Printf("%0.3f", root)
		}

	*/
	/*
		quotient, err := p.Divide(5.6, 0.0)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Printf("%0.2f\n", quotient)
		}

	*/
	/*
		truth := true
		p.Negate(&truth)
		fmt.Println(truth)
		lies := false
		p.Negate(&lies)
		fmt.Println(lies)

	*/
	//p.TestArrays()
	//fmt.Println(p.Test(7, 9))
	//fmt.Println(p.Test(7, 9))
	//p.Test()
	//fmt.Println(p.TestArrays(71, 56.2, 89.5))
	//fmt.Println(p.TestArrays(90.7, 89.7, 98.5, 92.3))
	//p.Massiv()
	//p.OpenFile()
	/*
		numbers, err := p.GetFloats("data.txt")
		if err != nil {
			log.Fatal(err)
		}
		var sum float64 = 0
		for _, number := range numbers {
			sum += number
		}
		sampleCount := float64(len(numbers))
		fmt.Printf("Average: %0.2f\n", sum/sampleCount)

	*/

}
func Counts() {
	/* map
	grades := map[string]float64{
		"Alma":  74.2,
		"Rohin": 86.5,
		"Carl":  59.7,
	}
	var names []string
	for name := range grades {
		names = append(names, name)
	}
	sort.Strings(names)
	for _, name := range names {
		fmt.Printf("%s has a great of %0.1f%%\n", name, grades[name])
	}

	*/// Map

	lines, err := p.TestArr("votes.txt")
	if err != nil {
		log.Fatal(err)
	}
	counts := make(map[string]int)
	for _, line := range lines {
		counts[line]++
	}
	for name, count := range counts {
		fmt.Printf("Votes for %s: %d\n", name, count)
	}
}
